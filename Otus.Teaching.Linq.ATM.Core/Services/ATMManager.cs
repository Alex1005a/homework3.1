﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }
        
        //TODO: Добавить методы получения данных для банкомата
        
        //Метод, отвечающий за вывод информации о пользователе 
        public void UserInfo()
        {
            System.Console.Write("Введите логин: ");
            string login = System.Console.ReadLine();
            System.Console.Write("Введите пароль: ");
            string pass = System.Console.ReadLine();

            var uss = from t in Users
                      where t.Login == login && t.Password == pass
                      select t;

            foreach (User s in uss)
            {
                System.Console.WriteLine(s.Id);
                System.Console.WriteLine(s.FirstName);
                System.Console.WriteLine(s.SurName);
                System.Console.WriteLine(s.MiddleName);
                System.Console.WriteLine(s.Phone);
                System.Console.WriteLine(s.PassportSeriesAndNumber);
                System.Console.WriteLine(s.RegistrationDate);
                System.Console.WriteLine(s.Login);
                System.Console.WriteLine(s.Password + "\n");
            }

            System.Console.WriteLine("У этого аккаунта такие счета:\n");
            AccInfo(uss);
            OutCash();
            Info(10000m);
        }

        //Метод, отвечающий за вывод информации о счетах пользователя
        public void AccInfo(IEnumerable<User> user)
        {
            int id = 0;
            foreach (User s in user)
            {
                id = s.Id;
            }

            var acc = from t in Accounts
                      where t.UserId == id
                      select t;

            foreach (Account s in acc)
            {
                System.Console.WriteLine(s.Id);
                System.Console.WriteLine(s.OpeningDate);
                System.Console.WriteLine(s.CashAll);
                System.Console.WriteLine(s.UserId + "\n");
                System.Console.WriteLine("История по этому счету:");
                OperInfo(s);
            }

        }

        //Метод, отвечающий за вывод информации о операциях над счетом 
        public void OperInfo(Account accounts)
        {
            var his = from t in History
                      where t.AccountId == accounts.Id
                      select t;

            foreach (OperationsHistory s in his)
            {
                System.Console.WriteLine(s.Id);
                System.Console.WriteLine(s.OperationDate);
                System.Console.WriteLine(s.OperationType);
                System.Console.WriteLine(s.CashSum);
                System.Console.WriteLine(s.AccountId + "\n");
            }
        }

        //Метод, отвечающий за вывод информации о всех операциях пополнения всех счетов
        public void OutCash()
        {
            var his = from t in History
                      where t.OperationType == OperationType.OutputCash
                      select t;

            foreach (OperationsHistory s in his)
            {
                System.Console.WriteLine(s.Id);
                System.Console.WriteLine(s.OperationDate);
                System.Console.WriteLine(s.OperationType);
                System.Console.WriteLine(s.CashSum);
                System.Console.WriteLine(s.AccountId + "\n");

                var uss = from t in Users
                        from r in Accounts
                        where t.Id == r.UserId && r.Id== s.AccountId
                        select t;

                System.Console.WriteLine("Владелец счета:");

                foreach (User w in uss)
                {
                    System.Console.WriteLine(w.FirstName + "\n");
                }
            }
        }

        //Метод, отвечающий за вывод информации о всех пользователях, у которых на счете 
        //Больше N
        public void Info(decimal cash)
        {
            var uss = from t in Users
                      from r in Accounts
                      where t.Id == r.UserId && r.CashAll > cash
                      select t;

            System.Console.WriteLine($"У этих людей больше {cash} на счете:");
            foreach (User s in uss)
            {
                System.Console.WriteLine(s.FirstName);
                System.Console.WriteLine(s.SurName);
                System.Console.WriteLine(s.MiddleName + "\n");
            }
        }
    }
}